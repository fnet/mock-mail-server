package jcode.project.mailserver.config;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

public class CustomEmbeddedServletContainerCustomizer implements EmbeddedServletContainerCustomizer {

    @Override
    public void customize(ConfigurableEmbeddedServletContainer container) {
        try {
            container.setAddress(InetAddress.getByName("0.0.0.0"));
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
        container.setPort(9000);
        container.setSessionTimeout(30, TimeUnit.MINUTES);

    }

}
